import numpy as np
import matplotlib.pyplot as plt
import queue
from itertools import product

BACKGROUND = '#292929'
FOREGROUND = '#f7dbc4'
MARKERS = [
    "o", "v", "^", "<", ">", "X", "D", "d", 
    "8", "s", "p", "P", "*", "h", "H"]

class Couple():
    def __init__(self, low, high, existing_points=None):
        self.path = None
        self.grid_cost = None
        self.point_a = None
        self.point_b = None
        self.complexity = None
        self.get_couple(low, high, existing_points=existing_points)

    def get_couple(self, low, high, existing_points=None):

        point_a = get_unique_point(low, high, existing_points=existing_points)
        if existing_points is not None:
            existing_points.append(point_a)
        else:
            existing_points = [point_a]

        point_b = get_unique_point(low, high, existing_points=existing_points)

        self.point_a = point_a
        self.point_b = point_b

    def update_complexity(self, array):

        xmin = min(self.point_a[0], self.point_b[0])
        ymin = min(self.point_a[1], self.point_b[1])
        xmax = max(self.point_a[0], self.point_b[0])
        ymax = max(self.point_a[1], self.point_b[1])
        self.complexity = np.sum(array[xmin: xmax, ymin:ymax])
        if self.path is not None:
            self.complexity += 1e6

    def show(self):

        print(self.point_b, "=>", self.point_a)
        if self.complexity is not None:
            print(self.complexity)
        if self.path is not None:
            print(self.path)

class Couples():
    def __init__(self, low, high, number_of_couples):

        self.couples = []
        self.point_list = []

        for _ in range(0, number_of_couples):
            self.add_couple(low, high, existing_points=self.point_list)
            self.update_point_list()

    def add_couple(self, low, high, existing_points=None):

        couple = Couple(low, high, existing_points=existing_points)
        self.couples.append(couple)

    def update_point_list(self):

        point_list = []

        for couple in self.couples:
            point_list.append(couple.point_a)
            point_list.append(couple.point_b)

        self.point_list = point_list

    def order(self):

        complexities = []

        for couple in self.couples:
            complexity = couple.complexity
            complexities.append(complexity)

        ordered_couples = sorted(self.couples, key=lambda couple: couple.complexity)

        self.couples = ordered_couples

    def update_complexity(self, array):

        for couple in self.couples:
            couple.update_complexity(array)

    def show(self):

        print("========")
        for couple in self.couples:
            couple.show()

    def plot(self, grid_size):
        fig, ax = plt.subplots(figsize=(6, 6))
        ax.set_facecolor(BACKGROUND)
        ax.set_xlim([0, grid_size])
        ax.set_ylim([0, grid_size])
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        for index, couple in enumerate(self.couples):
            plt.scatter(
                [couple.point_a[1], couple.point_b[1]],
                [couple.point_a[0], couple.point_b[0]],
                c=FOREGROUND, s=100, marker=MARKERS[index%len(MARKERS)])
        

class Grid:
    def __init__(self, size=10, couples=None):
        self.grid_size = size
        self.array = np.zeros((grid_size, grid_size))
        self.couples = couples
        if self.couples is None:
            self.couples = Couples(0, self.grid_size, 0)
        self.update_grid_from_couples()
        self.order_couples()
        self.grid_cost = self.array

    def find_paths(self, show=False):
        for _, couple in enumerate(self.couples.couples):
            if couple.path is None:
                couple.grid_cost = self.reach(couple.point_a, couple.point_b)
                couple.path = self.find_path(couple)
                self.update_array(couple.path)
                self.order_couples()
                if show:
                    self.show(show_cost=False)

    def add_couples(self, n_couples=10, margin=2):

        for _ in range(0, n_couples):
            obstacles = list(zip(*(np.nonzero(self.array == 1))))
            self.couples.add_couple(margin, self.grid_size - margin, existing_points=obstacles)
            self.update_grid_from_couples()


    def order_couples(self):
        if self.couples is not None:
            self.couples.update_complexity(self.array)
            self.couples.order()

    def update_grid_from_couples(self):

        if self.couples is not None:
            for couple in self.couples.couples:
                self.array[couple.point_a[0], couple.point_a[1]] = 1
                self.array[couple.point_b[0], couple.point_b[1]] = 1

    def update_array(self, points):

        for point in points:
            self.array[point[0], point[1]] = 1

    def show(self, show_cost=False, save=False, save_name='foo'):
        fig, ax = plt.subplots(figsize=(6, 6))
        ax.set_facecolor(BACKGROUND)
        ax.set_xlim([0, self.grid_size])
        ax.set_ylim([0, self.grid_size])
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        foreground_color = FOREGROUND
        if show_cost:
            foreground_color = 'white'
        for index, couple in enumerate(mygrid.couples.couples):
            plt.scatter(
                [couple.point_a[1], couple.point_b[1]],
                [couple.point_a[0], couple.point_b[0]],
                c=foreground_color, s=100, marker=MARKERS[index%len(MARKERS)])
            if couple.path is not None:
                if show_cost:
                    plt.imshow(couple.grid_cost, interpolation="nearest", cmap="viridis")
                plt.plot(
                    [y for _,y in couple.path], [x for x,_ in couple.path],
                    color=foreground_color)
        if save:
            plt.savefig(save_name + '.png', pad_inches=0.1, bbox_inches='tight', transparent=False)

    def find_path(self, couple):
        point = couple.point_b
        path = [point]
        max_iter = 0
        while (point != couple.point_a) and max_iter < 5000:
            max_iter += 1
            min_cost = 1e9
            next_point = None
            for idx, idy in [(-1, 0), (0, 1), (1, 0), (0, -1)]:
                visited_point = list(point)
                visited_point[0] += idx
                visited_point[1] += idy
                visited_point = tuple(visited_point)

                is_valid = True
                if (
                    (visited_point[0] >= self.grid_size)
                    or (visited_point[1] >= self.grid_size)
                    or (visited_point[0] < 0)
                    or (visited_point[1] < 0)
                ):
                    is_valid = False
                
                if is_valid:
                    if (
                        (couple.grid_cost[visited_point] > 0)
                        & (couple.grid_cost[visited_point] < min_cost)
                    ) or (visited_point == couple.point_a):
                        min_cost = couple.grid_cost[visited_point]
                        next_point = visited_point

            if next_point is None:
                break
            point = next_point
            path.append(point)

        return path

    def reach(self, start, destination):
        q = queue.Queue()
        q.put(start)
        point = start
        visited = self.array.astype(bool)
        visited[destination] = False
        grid_cost = np.zeros((self.grid_size, self.grid_size))
        # Do a Breadth First Search starting from start
        while (not q.empty()) and not (point == destination):

            for idx, idy in [(-1, 0), (0, 1), (1, 0), (0, -1)]:
                visited_point = list(point)
                visited_point[0] += idx
                visited_point[1] += idy
                visited_point = tuple(visited_point)

                is_valid = True
                if (
                    (visited_point[0] >= self.grid_size)
                    or (visited_point[1] >= self.grid_size)
                    or (visited_point[0] < 0)
                    or (visited_point[1] < 0)
                ):
                    is_valid = False

                if is_valid:
                    if not visited[visited_point]:
                        visited[visited_point] = True
                        grid_cost[visited_point] = grid_cost[point] + 1
                        q.put(visited_point)
                    if visited_point == destination:
                        break

            point = q.get()

        return grid_cost


def get_random_point(low, high):

    point = (np.random.randint(low, high), np.random.randint(low, high))

    return point


def get_unique_point(low, high, existing_points=None):

    point = get_random_point(low, high)

    if existing_points is not None:
        if len(existing_points) >= (grid_size * grid_size):
            raise ValueError
        existing_points.append(point)
    else:
        existing_points = [point]

    while point in existing_points:
        point = get_random_point(low, high)

    return point



if __name__ == "__main__":
    grid_size = 30
    n_couples =  10

    for i in range(0, 10):
        mygrid = Grid(size=grid_size)
        mygrid.add_couples(n_couples=20, margin=3)
        mygrid.order_couples()

        #mygrid.show()

        mygrid.find_paths(show=False)

        mygrid.show(show_cost=False, save=True, save_name=str(i))

